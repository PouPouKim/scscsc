package model;

import java.io.File;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.filechooser.FileSystemView;
import javax.swing.table.AbstractTableModel;

public class MyFileTableModel extends AbstractTableModel {

	private File[] files;
	private FileSystemView fileSystemView = FileSystemView.getFileSystemView();
	private String[] columns = { "Icon", "File", "Type", "Size", "Last Modified" };

	public MyFileTableModel() {
		this(new File[0]);
	}

	MyFileTableModel(File[] files) {
		this.files = files;
	}

	public int getColumnCount() {
		return columns.length;
	}

	public Class<?> getColumnClass(int column) {

		switch (column) {
		case 0:// Icon
			return ImageIcon.class;
		case 1: // File
			return String.class;
		case 2: //Type
			return String.class;
		case 3: // Size
			return String.class;
		case 4://Last Modified
			return String.class;
		}
		return null;
	}

	public String getColumnName(int column) {
		return columns[column];
	}

	public int getRowCount() {
		return files.length;
	}

	public File getFile(int row) {
		return files[row];
	}

	public void setFiles(File[] files) {
		this.files = files;
		fireTableDataChanged();
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		super.setValueAt(aValue, rowIndex, columnIndex);
	}

	@Override
	public Object getValueAt(int row, int column) {
		File file = files[row];
		
		switch (column) {
		case 0:
			return fileSystemView.getSystemIcon(file);
		case 1:
			return fileSystemView.getSystemDisplayName(file);
		case 2:
			return URLConnection.guessContentTypeFromName(file.getName());
		case 3:
			return readableFileSize(file.length());
		case 4:{
			SimpleDateFormat sfd = new SimpleDateFormat("dd/MM/yyyy hh:mm");

			return sfd.format(new Date(file.lastModified()));
		}
		default:
			System.err.println("Logic Error");
		}
		return "";
	}
	
	//File size formatting function
	private String readableFileSize(long size) {
	    if(size <= 0) return "";
	    final String[] units = new String[] { "bytes", "KB", "MB", "GB", "TB" };
	    int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
	    return new DecimalFormat("#,##0.#").format(size/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
	}
}
