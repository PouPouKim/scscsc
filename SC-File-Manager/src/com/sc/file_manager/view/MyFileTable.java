package view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileSystemView;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import model.MyFileTableModel;

public class MyFileTable extends JTable {
	private FileSystemView fileSystemView = FileSystemView.getFileSystemView();
	private final int iconPadding = 6;
	private boolean isTableUISet = false;
	public MainFrame superView;
	private MyFileTable table = this;
	private MyMouseListener mouseListener;

	public MyFileTable() {
		this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		//for clicking event
		mouseListener = new MyMouseListener();
		this.addMouseListener(mouseListener);
		
		this.setAutoCreateRowSorter(false);
		this.setShowVerticalLines(true);
		this.setGridColor(Color.LIGHT_GRAY);

		//for border empty space
		DefaultTableCellRenderer borderRenderer = new DefaultTableCellRenderer() {
			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
					boolean hasFocus, int row, int column) {
				super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				setBorder(
						BorderFactory.createCompoundBorder(getBorder(), BorderFactory.createEmptyBorder(0, 10, 0, 0)));
				return this;
			}

		};
		this.setDefaultRenderer(String.class, borderRenderer);
	}
	
	private class MyMouseListener extends MouseAdapter{
		@Override
		public void mouseClicked(MouseEvent me) {
			try {
				int clickedRow = table.getSelectionModel().getLeadSelectionIndex();
				File clickedFile = ((MyFileTableModel) table.getModel()).getFile(clickedRow);
				
				//one click
				if(me.getClickCount() == 1){
					superView.setFileDetails(clickedFile);
				}
				//double click
				else if(me.getClickCount() == 2){
					if(clickedFile.isDirectory()){
						superView.openDirectory(clickedFile);
					}else{
						superView.openNoneDirectoryFile(clickedFile);
					}
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

		}
	}
	

	public void setTableData(final File[] files) {
		MyFileTable table = this;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				if (!isTableUISet) {
					Icon icon = fileSystemView.getSystemIcon(files[0]);
					table.setRowHeight(icon.getIconHeight() + iconPadding);

					double tableWidth = table.getSize().getWidth();
					setColumnWidth(0, (int) (0.04 * tableWidth));
					setColumnWidth(1, (int) (0.46 * tableWidth));
					setColumnWidth(2, (int) (0.18 * tableWidth));
					setColumnWidth(3, (int) (0.12 * tableWidth));
					setColumnWidth(4, (int) (0.20 * tableWidth) + 10);
				}
				isTableUISet = true;

				table.removeMouseListener(mouseListener);
				MyFileTableModel model = (MyFileTableModel) table.getModel();
				model.setFiles(files);
				table.addMouseListener(mouseListener);
			}
		});
	}

	private void setColumnWidth(int column, int width) {
		TableColumn tableColumn = this.getColumnModel().getColumn(column);
		tableColumn.setPreferredWidth(width);
		tableColumn.setMaxWidth(width);
		tableColumn.setMinWidth(width);
	}

}
