package view;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileSystemView;
import javax.swing.plaf.IconUIResource;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import actionListener.editMenu.*;
import actionListener.fileMenu.*;
import actionListener.viewMenu.*;
import etc.MyErrorMessage;
import etc.MyFileTreeCellRenderer;
import etc.NodeIcon;
import model.MyFileTableModel;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import java.awt.Dimension;
import javax.swing.JSplitPane;
import javax.swing.JScrollPane;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.JProgressBar;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.SwingConstants;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainFrame extends JFrame {
	// =======GUI Components========
	private JPanel contentPane;
	private JMenuBar menuBar;
	private JMenu fileMenu;
	private JMenuItem openFolderMenuItem;
	private JMenuItem openFileMenuItem;
	private JMenuItem newFolderMenuItem;
	private JMenuItem NewFileMenuItem;
	private JMenuItem deleteMenuItem;
	private JMenuItem exitMenuItem;
	private JMenu editMenu;
	private JMenuItem renameMenuItem;
	private JMenuItem copyMenuItem;
	private JMenuItem cutMenuItem;
	private JMenuItem pasteMenuItem;
	private JMenu viewMenu;
	private JMenuItem detailsViewMenuItem;
	private JMenuItem listViewMenuItem;
	private JMenuItem iconsViewMenuItem;
	private JMenuItem titlesViewMenuItem;
	private JMenu helpMenu;
	private JMenuItem shortcutsMenuItem;
	private JTextField locationTextField;
	private JSplitPane splitPane;
	private JScrollPane fileTreeScrollPane;
	private MyFileTree fileTree;
	private JScrollPane fileViewScrollPane;
	private MyFileTable fileTable;
	// ===========================
	private DefaultTreeModel fileTreeModel;
	private MyFileTableModel fileTableModel;
	private FileSystemView fileSystemView = FileSystemView.getFileSystemView();
	private JProgressBar progressBar;
	private JTextField fileNameTextField;
	private JTextField filePathTextField;
	private JLabel fileDateValueLabel;
	private JLabel fileSizeValueLabel;

	private File curFile;
	private JLabel iconLabel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {

		// ================GUI CODE================
		setTitle("SC-File-Manager");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 971, 677);

		this.menuBar = new JMenuBar();
		setJMenuBar(this.menuBar);

		this.fileMenu = new JMenu("File");
		this.menuBar.add(this.fileMenu);

		this.openFolderMenuItem = new JMenuItem("Open Folder");
		this.openFolderMenuItem.addActionListener(new OpenFolderMenuActionListener());
		this.fileMenu.add(this.openFolderMenuItem);

		this.openFileMenuItem = new JMenuItem("Open File");
		this.openFileMenuItem.addActionListener(new OpenFileMenuActionListener());
		this.fileMenu.add(this.openFileMenuItem);

		this.newFolderMenuItem = new JMenuItem("New Folder");
		this.newFolderMenuItem.addActionListener(new NewFolderMenuActionListener());
		this.fileMenu.add(this.newFolderMenuItem);

		this.NewFileMenuItem = new JMenuItem("New File");
		this.NewFileMenuItem.addActionListener(new NewFileMenuActionListener());
		this.fileMenu.add(this.NewFileMenuItem);

		this.deleteMenuItem = new JMenuItem("Delete");
		this.deleteMenuItem.addActionListener(new DeleteMenuActionListener());
		this.fileMenu.add(this.deleteMenuItem);

		this.exitMenuItem = new JMenuItem("Exit");
		this.exitMenuItem.addActionListener(new ExitMenuActionListener());
		this.fileMenu.add(this.exitMenuItem);

		this.editMenu = new JMenu("Edit");
		this.menuBar.add(this.editMenu);

		this.renameMenuItem = new JMenuItem("Rename");
		this.renameMenuItem.addActionListener(new RenameMenuActionListener());
		this.editMenu.add(this.renameMenuItem);

		this.copyMenuItem = new JMenuItem("Copy");
		this.copyMenuItem.addActionListener(new CopyMenuActionListener());
		this.editMenu.add(this.copyMenuItem);

		this.cutMenuItem = new JMenuItem("Cut");
		this.cutMenuItem.addActionListener(new CutMenuActionListener());
		this.editMenu.add(this.cutMenuItem);

		this.pasteMenuItem = new JMenuItem("Paste");
		this.pasteMenuItem.addActionListener(new PasteMenuActionListener());
		this.editMenu.add(this.pasteMenuItem);

		this.viewMenu = new JMenu("View");
		this.menuBar.add(this.viewMenu);

		this.detailsViewMenuItem = new JMenuItem("Details");
		this.detailsViewMenuItem.addActionListener(new DetailsViewMenuActionListener());
		this.viewMenu.add(this.detailsViewMenuItem);

		this.listViewMenuItem = new JMenuItem("List");
		this.listViewMenuItem.addActionListener(new ListViewMenuActionListener());
		this.viewMenu.add(this.listViewMenuItem);

		this.iconsViewMenuItem = new JMenuItem("Icons");
		this.iconsViewMenuItem.addActionListener(new IconsViewMenuActionListener());
		this.viewMenu.add(this.iconsViewMenuItem);

		this.titlesViewMenuItem = new JMenuItem("Titles");
		this.viewMenu.add(this.titlesViewMenuItem);

		this.helpMenu = new JMenu("Help");
		this.menuBar.add(this.helpMenu);

		this.shortcutsMenuItem = new JMenuItem("Shortcuts");
		this.helpMenu.add(this.shortcutsMenuItem);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(this.contentPane);

		JPanel topPanel = new JPanel();
		this.contentPane.add(topPanel, BorderLayout.NORTH);
		GridBagLayout gbl_topPanel = new GridBagLayout();
		gbl_topPanel.columnWidths = new int[] { 72, 116, 30 };
		gbl_topPanel.rowHeights = new int[] { 24, 0 };
		gbl_topPanel.columnWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		gbl_topPanel.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		topPanel.setLayout(gbl_topPanel);

		JLabel locationLabel = new JLabel("Location : ");
		GridBagConstraints gbc_locationLabel = new GridBagConstraints();
		gbc_locationLabel.anchor = GridBagConstraints.WEST;
		gbc_locationLabel.insets = new Insets(0, 0, 0, 5);
		gbc_locationLabel.gridx = 0;
		gbc_locationLabel.gridy = 0;
		topPanel.add(locationLabel, gbc_locationLabel);

		this.locationTextField = new JTextField();
		locationTextField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File f = new File(locationTextField.getText());
				if (f.isDirectory()) {
					openDirectory(f);
				} else {
					openNoneDirectoryFile(f);
				}
			}
		});

		GridBagConstraints gbc_locationTextField = new GridBagConstraints();
		gbc_locationTextField.gridwidth = 2;
		gbc_locationTextField.fill = GridBagConstraints.BOTH;
		gbc_locationTextField.anchor = GridBagConstraints.WEST;
		gbc_locationTextField.gridx = 1;
		gbc_locationTextField.gridy = 0;
		topPanel.add(this.locationTextField, gbc_locationTextField);
		this.locationTextField.setColumns(10);

		this.splitPane = new JSplitPane();
		this.contentPane.add(this.splitPane, BorderLayout.CENTER);

		// Set default tree collapsed icon
		UIManager.put("Tree.collapsedIcon", new IconUIResource(new NodeIcon('+')));
		UIManager.put("Tree.expandedIcon", new IconUIResource(new NodeIcon('-')));

		this.loadSystemFiles();
		this.fileTree = new MyFileTree(this.fileTreeModel);
		this.fileTree.superView = this;
		this.fileTree.setRootVisible(false);
		this.fileTree.setCellRenderer(new MyFileTreeCellRenderer());
		this.fileTree.expandRow(0);

		this.fileTreeScrollPane = new JScrollPane(this.fileTree);
		this.splitPane.setLeftComponent(this.fileTreeScrollPane);

		Dimension FTWidthPreferred = new Dimension(230, (int) this.fileTreeScrollPane.getPreferredSize().getHeight());
		this.fileTreeScrollPane.setPreferredSize(FTWidthPreferred);

		JPanel fileViewPane = new JPanel();
		this.splitPane.setRightComponent(fileViewPane);

		this.fileTableModel = new MyFileTableModel();
		GridBagLayout gbl_fileViewPane = new GridBagLayout();
		gbl_fileViewPane.columnWidths = new int[] { 697, 0 };
		gbl_fileViewPane.rowHeights = new int[] { 450, 120, 0 };
		gbl_fileViewPane.columnWeights = new double[] { 0.0, Double.MIN_VALUE };
		gbl_fileViewPane.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		fileViewPane.setLayout(gbl_fileViewPane);

		this.fileTable = new MyFileTable();
		this.fileTable.superView = this;
		this.fileTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.fileTable.setModel(fileTableModel);

		this.fileViewScrollPane = new JScrollPane(this.fileTable);
		Dimension d = this.fileViewScrollPane.getPreferredSize();
		this.fileViewScrollPane.setPreferredSize(new Dimension(452, 351));
		GridBagConstraints gbc_fileViewScrollPane = new GridBagConstraints();
		gbc_fileViewScrollPane.fill = GridBagConstraints.BOTH;
		gbc_fileViewScrollPane.insets = new Insets(0, 0, 5, 0);
		gbc_fileViewScrollPane.gridx = 0;
		gbc_fileViewScrollPane.gridy = 0;
		fileViewPane.add(this.fileViewScrollPane, gbc_fileViewScrollPane);

		JPanel fileDetailsPane = new JPanel();
		GridBagConstraints gbc_fileDetailsPane = new GridBagConstraints();
		gbc_fileDetailsPane.fill = GridBagConstraints.HORIZONTAL;
		gbc_fileDetailsPane.gridx = 0;
		gbc_fileDetailsPane.gridy = 1;
		fileViewPane.add(fileDetailsPane, gbc_fileDetailsPane);
		GridBagLayout gbl_fileDetailsPane = new GridBagLayout();
		gbl_fileDetailsPane.columnWidths = new int[] { 130, 0, 130 };
		gbl_fileDetailsPane.rowHeights = new int[] { 30, 30, 30, 30 };
		gbl_fileDetailsPane.columnWeights = new double[] { 0.0, 0.0, 1.0 };
		gbl_fileDetailsPane.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		fileDetailsPane.setLayout(gbl_fileDetailsPane);

		JLabel fileNameLabel = new JLabel("Name: ");
		GridBagConstraints gbc_fileNameLabel = new GridBagConstraints();
		gbc_fileNameLabel.anchor = GridBagConstraints.EAST;
		gbc_fileNameLabel.insets = new Insets(0, 0, 5, 5);
		gbc_fileNameLabel.gridx = 0;
		gbc_fileNameLabel.gridy = 0;
		fileDetailsPane.add(fileNameLabel, gbc_fileNameLabel);

		this.iconLabel = new JLabel("");
		GridBagConstraints gbc_iconLabel = new GridBagConstraints();
		gbc_iconLabel.insets = new Insets(0, 0, 5, 5);
		gbc_iconLabel.anchor = GridBagConstraints.EAST;
		gbc_iconLabel.gridx = 1;
		gbc_iconLabel.gridy = 0;
		fileDetailsPane.add(this.iconLabel, gbc_iconLabel);

		this.fileNameTextField = new JTextField();
		GridBagConstraints gbc_fileNameTextField = new GridBagConstraints();
		gbc_fileNameTextField.gridwidth = 2;
		gbc_fileNameTextField.insets = new Insets(2, 0, 5, 0);
		gbc_fileNameTextField.fill = GridBagConstraints.BOTH;
		gbc_fileNameTextField.gridx = 2;
		gbc_fileNameTextField.gridy = 0;
		fileDetailsPane.add(this.fileNameTextField, gbc_fileNameTextField);
		this.fileNameTextField.setColumns(10);

		JLabel filePathLabel = new JLabel("Path: ");
		GridBagConstraints gbc_filePathLabel = new GridBagConstraints();
		gbc_filePathLabel.anchor = GridBagConstraints.EAST;
		gbc_filePathLabel.insets = new Insets(0, 0, 5, 5);
		gbc_filePathLabel.gridx = 0;
		gbc_filePathLabel.gridy = 1;
		fileDetailsPane.add(filePathLabel, gbc_filePathLabel);

		this.filePathTextField = new JTextField();
		GridBagConstraints gbc_filePathTextField = new GridBagConstraints();
		gbc_filePathTextField.insets = new Insets(2, 0, 5, 0);
		gbc_filePathTextField.gridwidth = 3;
		gbc_filePathTextField.fill = GridBagConstraints.BOTH;
		gbc_filePathTextField.gridx = 1;
		gbc_filePathTextField.gridy = 1;
		fileDetailsPane.add(this.filePathTextField, gbc_filePathTextField);
		this.filePathTextField.setColumns(10);

		JLabel fileDateLabel = new JLabel("Last Modified: ");
		GridBagConstraints gbc_fileDateLabel = new GridBagConstraints();
		gbc_fileDateLabel.anchor = GridBagConstraints.EAST;
		gbc_fileDateLabel.insets = new Insets(0, 0, 5, 5);
		gbc_fileDateLabel.gridx = 0;
		gbc_fileDateLabel.gridy = 2;
		fileDetailsPane.add(fileDateLabel, gbc_fileDateLabel);

		this.fileDateValueLabel = new JLabel("St Oct 01 23 13");
		this.fileDateValueLabel.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_fileDateValueLabel = new GridBagConstraints();
		gbc_fileDateValueLabel.anchor = GridBagConstraints.WEST;
		gbc_fileDateValueLabel.gridwidth = 3;
		gbc_fileDateValueLabel.insets = new Insets(0, 0, 5, 0);
		gbc_fileDateValueLabel.gridx = 1;
		gbc_fileDateValueLabel.gridy = 2;
		fileDetailsPane.add(this.fileDateValueLabel, gbc_fileDateValueLabel);

		JLabel fileSizeLabel = new JLabel("File Size: ");
		GridBagConstraints gbc_fileSizeLabel = new GridBagConstraints();
		gbc_fileSizeLabel.anchor = GridBagConstraints.EAST;
		gbc_fileSizeLabel.insets = new Insets(0, 0, 0, 5);
		gbc_fileSizeLabel.gridx = 0;
		gbc_fileSizeLabel.gridy = 3;
		fileDetailsPane.add(fileSizeLabel, gbc_fileSizeLabel);

		this.fileSizeValueLabel = new JLabel("1651513123 byte");
		GridBagConstraints gbc_fileSizeValueLabel = new GridBagConstraints();
		gbc_fileSizeValueLabel.anchor = GridBagConstraints.WEST;
		gbc_fileSizeValueLabel.gridwidth = 2;
		gbc_fileSizeValueLabel.insets = new Insets(0, 0, 0, 5);
		gbc_fileSizeValueLabel.gridx = 1;
		gbc_fileSizeValueLabel.gridy = 3;
		fileDetailsPane.add(this.fileSizeValueLabel, gbc_fileSizeValueLabel);

		progressBar = new JProgressBar();
		GridBagConstraints gbc_progressBar = new GridBagConstraints();
		gbc_progressBar.gridx = 3;
		gbc_progressBar.gridy = 3;
		fileDetailsPane.add(this.progressBar, gbc_progressBar);
		progressBar.setValue(100);
		// ============================================
	}

	private void loadSystemFiles() {
		DefaultMutableTreeNode treeNode = new DefaultMutableTreeNode();
		fileTreeModel = new DefaultTreeModel(treeNode);

		// load all files from system root.
		File[] sysRoots = fileSystemView.getRoots();
		for (File fileSystemRoot : sysRoots) {
			DefaultMutableTreeNode node = new DefaultMutableTreeNode(fileSystemRoot);
			treeNode.add(node);

			File[] files = fileSystemView.getFiles(fileSystemRoot, true);
			for (File file : files) {
				if (file.isDirectory()) {
					node.add(new DefaultMutableTreeNode(file));
				}
			}
		}
	}

	public void loadChildNode(final DefaultMutableTreeNode node) {
		this.fileTree.setEnabled(false);
		// progressBar.setVisible(true);
		// progressBar.setIndeterminate(true);

		SwingWorker<Void, File> worker = new SwingWorker<Void, File>() {
			@Override
			public Void doInBackground() {
				File file = (File) node.getUserObject();
				if (file.isDirectory()) {
					File[] files = fileSystemView.getFiles(file, true); // !!
					if (node.isLeaf()) {
						for (File child : files) {
							if (child.isDirectory()) {
								publish(child);
							}
						}
					}
					fileTable.setTableData(files);
				}
				return null;
			}

			@Override
			protected void process(List<File> chunks) {
				for (File child : chunks) {
					node.add(new DefaultMutableTreeNode(child));
				}
			}

			@Override
			protected void done() {
				// progressBar.setIndeterminate(false);
				// progressBar.setVisible(false);
				fileTree.setEnabled(true);
			}
		};
		worker.execute();
	}

	public void openDirectory(File file) {
		DefaultMutableTreeNode node = new DefaultMutableTreeNode(file);
		loadChildNode(node);
	}

	public void openNoneDirectoryFile(File file) {
		Desktop desktop = Desktop.getDesktop();
		try {
			desktop.open(file);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(this, MyErrorMessage.CANNOT_OPEN_THE_FILE, "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	public void setFileDetails(File file) {
		fileNameTextField.setText(fileSystemView.getSystemDisplayName(file));
		filePathTextField.setText(file.getPath());
		fileDateValueLabel.setText(new Date(file.lastModified()).toString());
		fileSizeValueLabel.setText(file.length() + " bytes");
		locationTextField.setText(file.getPath() + "");

		Icon icon = fileSystemView.getSystemIcon(file);
		this.iconLabel.setIcon(icon);
	}

}
