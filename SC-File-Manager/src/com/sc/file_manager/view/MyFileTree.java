package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.io.File;

import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import etc.MyFileTreeCellRenderer;

public class MyFileTree extends JTree {
	//private FileSystemView fileSystemView = FileSystemView.getFileSystemView();
	public MainFrame superView;
	
	
	public MyFileTree(DefaultTreeModel m) {
		super(m);
		
		this.setCellRenderer(new MyFileTreeCellRenderer()); // 이미지 바꾸는 작업
		this.addTreeSelectionListener(new MySelectionListener());
		

	}
	
	private class MySelectionListener implements TreeSelectionListener {

		@Override
		public void valueChanged(TreeSelectionEvent e) {
			// TODO Auto-generated method stub
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.getPath().getLastPathComponent();
			superView.loadChildNode(node);
			superView.setFileDetails((File)node.getUserObject());
		}
				
		/*
		@Override
		public void valueChanged(TreeSelectionEvent e) {
			// TODO Auto-generated method stub

			// 노드 추가..
			TreePath path = e.getPath(); // 선택 경로 저장하는 ..
			Object[] nodes = path.getPath(); // 저장된 각각의 경로를 오브젝트로 저장
			String filepath = "";
			for (int k = 0; k < nodes.length; k++) {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode) nodes[k];
				System.out.println(nodes[k]);
				if (node.getUserObject() instanceof Directory) { // 디렉토리는
																	// '\'(구분자)만
																	// 붙임
					filepath += node.toString() + System.getProperty("file.separator");
				} else if (node.getUserObject() instanceof Driver) { // 드라이버는
																		// 'c:\'
																		// 까지 붙임
					filepath += node.toString();
				}
			}
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) nodes[nodes.length - 1];
			// 배열은 0부터 이므로 자기 현재 위치를 정하기 위해 -1 해준다. 배열은 0부터, but 노드는 1부터~
			File f1 = new File(filepath);
			File f2 = null;
			String list[] = f1.list();
			if (list != null) {
				for (int i = 0; i < list.length; i++) {
					f2 = new File(filepath + list[i]);
					if (f2.isDirectory()) {
						DefaultMutableTreeNode dmt1 = new DefaultMutableTreeNode(new Directory(list[i]));
						node.add(dmt1);
					}
				}
			}

		}*/

		

	}

}